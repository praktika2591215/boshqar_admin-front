export default {
    computed: {
        pages() {
            return [
                {id: 1, name: 'Barcha mijozlar', url: '/'},
                {id: 2, name: 'Mijozlar statistikasi', url: '/statistics'},
                {id: 3, name: 'Mijozlar joylashuvi va to\'lov', url: '/charts'},
                {id: 4, name: 'Mijozlar oqimi', url: '/trade-statistics'},
                {id: 5, name: 'Mijozlar bilan bog\'lanish', url: '#'},
                {id: 6, name: 'To\'lov turi', url: '/type-payment'}
            ];
        }
    }
}
