import {createRouter, createWebHistory} from "vue-router"
import HomePage from "@/pages/HomePage"

const routes = [
    {
        path: '/',
        component: HomePage
    },
    {
        path: '/statistics',
        component: () => import('../pages/StatisticPage.vue')
    },
    {
        path: '/charts',
        component: () => import('../pages/ChartsPage.vue')
    },
    {
        path: '/trade-statistics',
        component: () => import('../pages/ChartsTradeStatistics.vue')
    },
    {
        path: '/type-payment',
        component: () => import('../components/ChartsPaymentType.vue')
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})
